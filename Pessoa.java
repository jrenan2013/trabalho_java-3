// criando construtor publico para poder ser chamado por qualquer classe.
//construtor recebe os nomes como parametros e os atribui aos atributos.
public class Pessoa {
    private String primeiroNome;
    private String ultimoNome;
    private String nomesDoMeio;
    
    
    public Pessoa(String primeiro, String meio, String ultimo) {
      primeiroNome = primeiro;
      ultimoNome = ultimo;
      nomesDoMeio = meio;
    }
    
    // metodo publico
    //retorna objeto do tipo string getNomeCompleto
    //nao recebe parametro
    public String getNomeCompleto() {
      // declarando nova variavel do tipo string
      //nomeCompleto, primeiroNome são variaveis locais
      String nomeCompleto = primeiroNome + " " + nomesDoMeio + " " + ultimoNome;
      return nomeCompleto;
    }
    
  }